import sys
from PySide6.QtWidgets import QApplication, QMainWindow, QListWidgetItem
from design.code.main import Ui_mainWindow
from trackWindow import TrackWindow
import requests
from Track import Track
from PIL import Image
import io

#https://open.spotify.com/playlist/3v2mcMXQ916fPVQGzViA46?si=88030f8a6e5a4dfc
#https://open.spotify.com/playlist/4IYXSQUWal9VhUnDC2fNRM?si=b637bb1fa3814b0c
#https://open.spotify.com/playlist/3Iez4rZiDjUVdWhnNbAsDa?si=e4b3bd78e0604d98

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_mainWindow()
        self.ui.setupUi(self)
        self.trackWindow = TrackWindow()

        self.ui.confirmPushButton.clicked.connect(self.comfirm)
        self.ui.doubleRadioButton.clicked.connect(self.onePlaylistMode)
        self.ui.differentRadioButton.clicked.connect(self.twoPlaylistMode)
        self.ui.communeRadioButton.clicked.connect(self.twoPlaylistMode)
        self.ui.artistCounterRadioButton.clicked.connect(self.onePlaylistMode)
        self.trackWindow.ui.nextPushButton.clicked.connect(self.next)
        self.trackWindow.ui.backPushButton.clicked.connect(self.back)
        self.trackWindow.ui.restartPushButton.clicked.connect(self.restart)

        self.ui.errorLabel.hide()

        self.commonTrack = []
        self.diffTrack = []
        self.doubleTrack = []
        self.artistCounter = []
        self.trackCommune = True
        self.trackDouble = False
        self.counterArtist = False

    def onePlaylistMode(self):
        self.ui.playlist2LineEdit.hide()
        self.ui.TextLabel2.hide()
        self.ui.TextLabel1.setText("Lien playlist:")

    def twoPlaylistMode(self):
        self.ui.playlist2LineEdit.show()
        self.ui.TextLabel2.show()
        self.ui.TextLabel1.setText("Lien playlist 1:")

    def comfirm(self):
        showTrack = True

        self.trackDouble = self.ui.doubleRadioButton.isChecked()

        if self.trackDouble:
            if self.ui.playlist1LineEdit.text() != "":
                doublon = self.getDoublonsInList(self.getAllMusiqueByLink(self.splitLinkToPlaylistId(self.ui.playlist1LineEdit.text())))

                if len(doublon) == 0:
                    self.ui.errorLabel.setText("Aucun titre en double!")
                    self.ui.errorLabel.show()
                    showTrack = False
                else:
                    self.doubleTrack = doublon
                    self.trackWindow.ui.indicateurLabel.setText("1 / " + str(len(self.doubleTrack)))
                    self.trackWindow.ui.titleLabel.setText(self.doubleTrack[0].getTitle())
                    self.trackWindow.ui.artistLabel.setText(self.doubleTrack[0].getArtist())
                    self.downloadReziseImageByLink(self.doubleTrack[0].getPictureLink())

        elif self.ui.artistCounterRadioButton.isChecked():
            if self.ui.playlist1LineEdit.text() != "":
                self.artistCounter = []
                currentPlaylist = self.getAllMusiqueByLink(self.splitLinkToPlaylistId(self.ui.playlist1LineEdit.text()))
                artistCounter = {}

                for track in currentPlaylist:
                    if track.getArtist() in artistCounter.keys():
                        artistCounter[track.getArtist()][0] = artistCounter[track.getArtist()][0] + 1
                    else:
                        artistCounter[track.getArtist()] = [1, track.getArtistId()]

                for key in artistCounter.keys():
                    self.artistCounter.append([key, artistCounter[key][0], artistCounter[key][1]])

                self.downloadReziseImageByLink(self.getArtistImageLinkById(self.artistCounter[0][2]))

                self.trackWindow.ui.indicateurLabel.setText("1 / " + str(len(self.artistCounter)))
                self.trackWindow.ui.titleLabel.setText(self.artistCounter[0][0])
                self.trackWindow.ui.artistLabel.setText(str(self.artistCounter[0][1]) + " musiques")
                self.trackWindow.ui.imageLabel.setPixmap("image.png")
                self.counterArtist = True
                self.trackCommune = False


            else:
                self.ui.errorLabel.setText("Veuillez saisir une playlist!")
                self.ui.errorLabel.show()
                showTrack = False
        else:
            if self.ui.playlist1LineEdit.text() != "" and self.ui.playlist2LineEdit.text() != "":
                listeMusiquePl1 = self.getAllMusiqueByLink(self.splitLinkToPlaylistId(self.ui.playlist1LineEdit.text()))
                listeMusiquePl2 = self.getAllMusiqueByLink(self.splitLinkToPlaylistId(self.ui.playlist2LineEdit.text()))

                self.setCommonTrack(listeMusiquePl1, listeMusiquePl2)
                self.setDiffTrack(listeMusiquePl1, listeMusiquePl2)

                self.trackCommune = self.ui.communeRadioButton.isChecked()

                if self.trackCommune:
                    if len(self.commonTrack) == 0:
                        self.ui.errorLabel.setText("Aucun titre en commun!")
                        self.ui.errorLabel.show()
                        showTrack = False
                    else:
                        self.trackWindow.ui.indicateurLabel.setText("1 / " + str(len(self.commonTrack)))
                        self.trackWindow.ui.titleLabel.setText(self.commonTrack[0].getTitle())
                        self.trackWindow.ui.artistLabel.setText(self.commonTrack[0].getArtist())
                        self.downloadReziseImageByLink(self.commonTrack[0].getPictureLink())
                else:
                    if len(self.diffTrack) == 0:
                        self.ui.errorLabel.setText("Aucun titre en différent!")
                        self.ui.errorLabel.show()
                        showTrack = False
                    else:
                        self.trackWindow.ui.indicateurLabel.setText("1 / " + str(len(self.diffTrack)))
                        self.trackWindow.ui.titleLabel.setText(self.diffTrack[0].getTitle())
                        self.trackWindow.ui.artistLabel.setText(self.diffTrack[0].getArtist())
                        self.downloadReziseImageByLink(self.diffTrack[0].getPictureLink())

        if showTrack:
            self.trackWindow.ui.imageLabel.setPixmap("image.png")
            self.trackWindow.ui.backPushButton.setDisabled(True)
            self.hide()
            self.trackWindow.show()


    def next(self):
        nextNBR = int(self.trackWindow.ui.indicateurLabel.text().split(" ")[0]) + 1
        if self.trackCommune:
            self.trackWindow.ui.indicateurLabel.setText(str(nextNBR) + " / " + str(len(self.doubleTrack)))
            self.trackWindow.ui.titleLabel.setText(self.doubleTrack[nextNBR - 1].getTitle())
            self.trackWindow.ui.artistLabel.setText(self.doubleTrack[nextNBR - 1].getArtist())
            self.downloadReziseImageByLink(self.doubleTrack[nextNBR - 1].getPictureLink())
            self.trackWindow.ui.imageLabel.setPixmap("image.png")

            if nextNBR == len(self.doubleTrack):
                self.trackWindow.ui.nextPushButton.setDisabled(True)
            else:
                self.trackWindow.ui.backPushButton.setDisabled(False)
        elif self.counterArtist:
            self.trackWindow.ui.indicateurLabel.setText(str(nextNBR) + " / " + str(len(self.artistCounter)))
            self.trackWindow.ui.titleLabel.setText(self.artistCounter[nextNBR - 1][0])
            self.trackWindow.ui.artistLabel.setText(str(self.artistCounter[nextNBR - 1][1]) + " musiques")
            self.downloadReziseImageByLink(self.getArtistImageLinkById(self.artistCounter[nextNBR - 1][2]))
            self.trackWindow.ui.imageLabel.setPixmap("image.png")

            if nextNBR == len(self.artistCounter):
                self.trackWindow.ui.nextPushButton.setDisabled(True)
            else:
                self.trackWindow.ui.backPushButton.setDisabled(False)
        else:
            if self.trackCommune:
                self.trackWindow.ui.indicateurLabel.setText(str(nextNBR) + " / " + str(len(self.commonTrack)))
                self.trackWindow.ui.titleLabel.setText(self.commonTrack[nextNBR - 1].getTitle())
                self.trackWindow.ui.artistLabel.setText(self.commonTrack[nextNBR - 1].getArtist())
                self.downloadReziseImageByLink(self.commonTrack[nextNBR - 1].getPictureLink())
                self.trackWindow.ui.imageLabel.setPixmap("image.png")

                if nextNBR == len(self.commonTrack):
                    self.trackWindow.ui.nextPushButton.setDisabled(True)
                else:
                    self.trackWindow.ui.backPushButton.setDisabled(False)
            else:
                self.trackWindow.ui.indicateurLabel.setText(str(nextNBR) + " / " + str(len(self.diffTrack)))
                self.trackWindow.ui.titleLabel.setText(self.diffTrack[nextNBR - 1].getTitle())
                self.trackWindow.ui.artistLabel.setText(self.diffTrack[nextNBR - 1].getArtist())
                self.downloadReziseImageByLink(self.diffTrack[nextNBR - 1].getPictureLink())
                self.trackWindow.ui.imageLabel.setPixmap("image.png")

                if nextNBR == len(self.diffTrack):
                    self.trackWindow.ui.nextPushButton.setDisabled(True)
                else:
                    self.trackWindow.ui.backPushButton.setDisabled(False)

    def back(self):
        backNBR = int(self.trackWindow.ui.indicateurLabel.text().split(" ")[0]) - 1

        if self.trackDouble:
            self.trackWindow.ui.indicateurLabel.setText(str(backNBR) + " / " + str(len(self.doubleTrack)))
            self.trackWindow.ui.titleLabel.setText(self.doubleTrack[backNBR - 1].getTitle())
            self.trackWindow.ui.artistLabel.setText(self.doubleTrack[backNBR - 1].getArtist())
            self.downloadReziseImageByLink(self.doubleTrack[backNBR - 1].getPictureLink())
            self.trackWindow.ui.imageLabel.setPixmap("image.png")
        elif self.counterArtist:
            self.trackWindow.ui.indicateurLabel.setText(str(backNBR) + " / " + str(len(self.artistCounter)))
            self.trackWindow.ui.titleLabel.setText(self.artistCounter[backNBR - 1][0])
            self.trackWindow.ui.artistLabel.setText(str(self.artistCounter[backNBR - 1][1]) + " musiques")
            self.downloadReziseImageByLink(self.getArtistImageLinkById(self.artistCounter[backNBR - 1][2]))
            self.trackWindow.ui.imageLabel.setPixmap("image.png")
        else:
            if self.trackCommune:
                self.trackWindow.ui.indicateurLabel.setText(str(backNBR) + " / " + str(len(self.commonTrack)))
                self.trackWindow.ui.titleLabel.setText(self.commonTrack[backNBR - 1].getTitle())
                self.trackWindow.ui.artistLabel.setText(self.commonTrack[backNBR - 1].getArtist())
                self.downloadReziseImageByLink(self.commonTrack[backNBR - 1].getPictureLink())
                self.trackWindow.ui.imageLabel.setPixmap("image.png")
            else:
                self.trackWindow.ui.indicateurLabel.setText(str(backNBR) + " / " + str(len(self.diffTrack)))
                self.trackWindow.ui.titleLabel.setText(self.diffTrack[backNBR - 1].getTitle())
                self.trackWindow.ui.artistLabel.setText(self.diffTrack[backNBR - 1].getArtist())
                self.downloadReziseImageByLink(self.diffTrack[backNBR - 1].getPictureLink())
                self.trackWindow.ui.imageLabel.setPixmap("image.png")

        if backNBR == 1:
            self.trackWindow.ui.backPushButton.setDisabled(True)
        else:
            self.trackWindow.ui.nextPushButton.setDisabled(False)

    def restart(self):
        self.commonTrack = []
        self.diffTrack = []
        self.doubleTrack = []
        self.trackWindow.hide()
        self.trackDouble = False
        self.trackCommune = True
        self.ui.communeRadioButton.setChecked(True)
        self.ui.playlist1LineEdit.setText("")
        self.ui.playlist2LineEdit.setText("")
        self.ui.errorLabel.hide()
        self.show()

    def getDoublonsInList(self, liste):
        newListe = []

        for item in liste:
            for otherItem in liste:
                if id(item) != id(otherItem) and item == otherItem:
                    newListe.append(item)
                    liste.remove(otherItem)

        return newListe

    def setCommonTrack(self, playlist1, playlist2):
        self.commonTrack = []
        for track in playlist1:
            if track in playlist2:
                self.commonTrack.append(track)

    def setDiffTrack(self, playlist1, playlist2):
        for track in playlist1:
            if not (track in playlist2):
                self.diffTrack.append(track)

        for track in playlist2:
            if not (track in playlist1):
                self.diffTrack.append(track)

        self.getDoublonsInList(self.diffTrack)

    def splitLinkToPlaylistId(self, link: str):
        return link.split("playlist")[1].split("/")[1].split("?")[0]

    def getArtistImageLinkById(self, idArtist: str):
        CLIENT_ID = '14a6e6efa28e42b99c31801888c8764b'
        CLIENT_SECRET = 'ebdb3a9ef04347b9b113322a8b972017'

        AUTH_URL = 'https://accounts.spotify.com/api/token'
        auth_response = requests.post(AUTH_URL, {'grant_type': 'client_credentials', 'client_id': CLIENT_ID,
                                                 'client_secret': CLIENT_SECRET})
        auth_response_data = auth_response.json()
        access_token = auth_response_data['access_token']

        headers = {'Authorization': 'Bearer {token}'.format(token=access_token)}
        BASE_URL = 'https://api.spotify.com/v1/'

        r = requests.get(BASE_URL + 'artists/' + idArtist, headers=headers,
                         params={'include_groups': 'album', 'limit': 100, 'offset': 0}).json()

        return r["images"][0]["url"]

    def getAllMusiqueByLink(self, idPlaylist: str):
        listeMusiquePlaylist = []

        CLIENT_ID = '14a6e6efa28e42b99c31801888c8764b'
        CLIENT_SECRET = 'ebdb3a9ef04347b9b113322a8b972017'

        AUTH_URL = 'https://accounts.spotify.com/api/token'
        auth_response = requests.post(AUTH_URL, {'grant_type': 'client_credentials', 'client_id': CLIENT_ID,
                                                 'client_secret': CLIENT_SECRET})
        auth_response_data = auth_response.json()
        access_token = auth_response_data['access_token']

        headers = {'Authorization': 'Bearer {token}'.format(token=access_token)}
        BASE_URL = 'https://api.spotify.com/v1/'

        ok = False
        offset = 0
        while not ok:
            r = requests.get(BASE_URL + 'playlists/' + idPlaylist + '/tracks', headers=headers,
                             params={'include_groups': 'album', 'limit': 100, 'offset': offset}).json()
            offset += 100

            newReceivedTrack = []

            for playlist in r['items']:
                newReceivedTrack.append(Track(playlist["track"]["name"], playlist["track"]["album"]["artists"][0]["name"], playlist["track"]["artists"][0]["id"], playlist["track"]["album"]["images"][0]["url"]))

            for track in newReceivedTrack:
                listeMusiquePlaylist.append(track)

            if len(newReceivedTrack) < 100:
                ok = True

        return listeMusiquePlaylist

    def downloadReziseImageByLink(self, link: str):
        r = requests.get(link, timeout=4.0)
        if r.status_code != requests.codes.ok:
            assert False, 'Status code error: {}.'.format(r.status_code)

        with Image.open(io.BytesIO(r.content)) as im:
            im.save("image.png")

        img = Image.open("image.png")
        img = img.resize((280, 280))
        img.save("image.png")

if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())