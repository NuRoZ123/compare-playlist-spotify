class Track:
    """Class Track

    Attributes
    ----------
    -title
    -artist
    -artistId
    -pictureLink

    methods
    -------
    +getTitle
    -setTitle
    +getArtist
    -setArtist
    +getArtistId
    -setArtistId

    """

    def __init__(self, title: str, artist: str, artistId: str, pictureLink: str):
        """Constructeur de la class Track
        """
        self.__setTitle(title)
        self.__setArtist(artist)
        self.__setArtistId(artistId)
        self.__setPictureLink(pictureLink)

    def getTitle(self) -> str:
        """getter sur l'attribut __title
        :return:
        """
        return self.__title

    def __setTitle(self, value: str) -> None:
        """setter sur l'attribut __title il permet sa valorisation
        :param value: str
        """
        self.__title = value

    def getArtist(self) -> str:
        """getter sur l'attribut __artist
        :return:
        """
        return self.__artist

    def __setArtist(self, value: str) -> None:
        """setter sur l'attribut __artist il permet sa valorisation
        :param value: str
        """
        self.__artist = value

    def getArtistId(self) -> str:
        """getter sur l'attribut artistId
        :return:
        """
        return self.artistId

    def __setArtistId(self, value: str) -> None:
        """setter sur l'attribut artistId il permet sa valorisation
        :param value: string
        """
        self.artistId = value

    def getPictureLink(self) -> str:
        """getter sur l'attribut __pictureLink
        :return:
        """
        return self.__pictureLink

    def __setPictureLink(self, value: str) -> None:
        """setter sur l'attribut __pictureLink il permet sa valorisation
        :param value: str
        """
        self.__pictureLink = value

    def __eq__(self, other):
        res = True

        if self.__title != other.getTitle() or self.__artist != other.getArtist():
            res = False

        return res

    def __str__(self) -> str:
        """retourne un str à afficher lors d'un print d'un object
        """
        return "title: " + self.getTitle() + " by: " + self.getArtist()