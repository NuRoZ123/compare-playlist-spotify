# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main.ui'
##
## Created by: Qt User Interface Compiler version 6.1.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore


class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        if not mainWindow.objectName():
            mainWindow.setObjectName(u"mainWindow")
        mainWindow.resize(634, 468)
        self.leavePushButton = QPushButton(mainWindow)
        self.leavePushButton.setObjectName(u"leavePushButton")
        self.leavePushButton.setGeometry(QRect(550, 440, 75, 24))
        self.verticalLayoutWidget = QWidget(mainWindow)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(170, 60, 301, 271))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.errorLabel = QLabel(self.verticalLayoutWidget)
        self.errorLabel.setObjectName(u"errorLabel")
        self.errorLabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.errorLabel)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.TextLabel1 = QLabel(self.verticalLayoutWidget)
        self.TextLabel1.setObjectName(u"TextLabel1")

        self.verticalLayout.addWidget(self.TextLabel1)

        self.playlist1LineEdit = QLineEdit(self.verticalLayoutWidget)
        self.playlist1LineEdit.setObjectName(u"playlist1LineEdit")

        self.verticalLayout.addWidget(self.playlist1LineEdit)

        self.TextLabel2 = QLabel(self.verticalLayoutWidget)
        self.TextLabel2.setObjectName(u"TextLabel2")

        self.verticalLayout.addWidget(self.TextLabel2)

        self.playlist2LineEdit = QLineEdit(self.verticalLayoutWidget)
        self.playlist2LineEdit.setObjectName(u"playlist2LineEdit")

        self.verticalLayout.addWidget(self.playlist2LineEdit)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.confirmPushButton = QPushButton(self.verticalLayoutWidget)
        self.confirmPushButton.setObjectName(u"confirmPushButton")

        self.verticalLayout.addWidget(self.confirmPushButton)

        self.verticalLayoutWidget_2 = QWidget(mainWindow)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(490, 10, 131, 100))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.communeRadioButton = QRadioButton(self.verticalLayoutWidget_2)
        self.communeRadioButton.setObjectName(u"communeRadioButton")
        self.communeRadioButton.setEnabled(True)
        self.communeRadioButton.setChecked(True)

        self.verticalLayout_2.addWidget(self.communeRadioButton)

        self.differentRadioButton = QRadioButton(self.verticalLayoutWidget_2)
        self.differentRadioButton.setObjectName(u"differentRadioButton")

        self.verticalLayout_2.addWidget(self.differentRadioButton)

        self.doubleRadioButton = QRadioButton(self.verticalLayoutWidget_2)
        self.doubleRadioButton.setObjectName(u"doubleRadioButton")

        self.verticalLayout_2.addWidget(self.doubleRadioButton)

        self.artistCounterRadioButton = QRadioButton(self.verticalLayoutWidget_2)
        self.artistCounterRadioButton.setObjectName(u"artistCounterRadioButton")

        self.verticalLayout_2.addWidget(self.artistCounterRadioButton)


        self.retranslateUi(mainWindow)
        self.leavePushButton.clicked.connect(mainWindow.close)

        QMetaObject.connectSlotsByName(mainWindow)
    # setupUi

    def retranslateUi(self, mainWindow):
        mainWindow.setWindowTitle(QCoreApplication.translate("mainWindow", u"Form", None))
        self.leavePushButton.setText(QCoreApplication.translate("mainWindow", u"quitter!", None))
        self.errorLabel.setText(QCoreApplication.translate("mainWindow", u"TextLabel", None))
        self.TextLabel1.setText(QCoreApplication.translate("mainWindow", u"Lien playlist 1:", None))
        self.TextLabel2.setText(QCoreApplication.translate("mainWindow", u"Lien playlist 2:", None))
        self.confirmPushButton.setText(QCoreApplication.translate("mainWindow", u"confirm\u00e9", None))
        self.communeRadioButton.setText(QCoreApplication.translate("mainWindow", u"track commune", None))
        self.differentRadioButton.setText(QCoreApplication.translate("mainWindow", u"track different", None))
        self.doubleRadioButton.setText(QCoreApplication.translate("mainWindow", u"double track", None))
        self.artistCounterRadioButton.setText(QCoreApplication.translate("mainWindow", u"artist counter", None))
    # retranslateUi

