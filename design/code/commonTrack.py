# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'commonTrack.ui'
##
## Created by: Qt User Interface Compiler version 6.0.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWidgets import *


class Ui_commonTrack(object):
    def setupUi(self, commonTrack):
        if not commonTrack.objectName():
            commonTrack.setObjectName(u"commonTrack")
        commonTrack.resize(631, 572)
        self.verticalLayoutWidget = QWidget(commonTrack)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(20, 340, 571, 101))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.titleLabel = QLabel(self.verticalLayoutWidget)
        self.titleLabel.setObjectName(u"titleLabel")
        font = QFont()
        font.setPointSize(22)
        self.titleLabel.setFont(font)
        self.titleLabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.titleLabel)

        self.artistLabel = QLabel(self.verticalLayoutWidget)
        self.artistLabel.setObjectName(u"artistLabel")
        self.artistLabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.artistLabel)

        self.imageLabel = QLabel(commonTrack)
        self.imageLabel.setObjectName(u"imageLabel")
        self.imageLabel.setGeometry(QRect(110, 40, 401, 261))
        self.imageLabel.setAlignment(Qt.AlignCenter)
        self.horizontalLayoutWidget = QWidget(commonTrack)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(10, 510, 611, 80))
        self.horizontalLayout_2 = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.restartPushButton = QPushButton(self.horizontalLayoutWidget)
        self.restartPushButton.setObjectName(u"restartPushButton")

        self.horizontalLayout_2.addWidget(self.restartPushButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.leavePushButton = QPushButton(self.horizontalLayoutWidget)
        self.leavePushButton.setObjectName(u"leavePushButton")

        self.horizontalLayout_2.addWidget(self.leavePushButton)

        self.widget = QWidget(commonTrack)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(120, 450, 359, 49))
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.backPushButton = QPushButton(self.widget)
        self.backPushButton.setObjectName(u"backPushButton")

        self.horizontalLayout.addWidget(self.backPushButton)

        self.indicateurLabel = QLabel(self.widget)
        self.indicateurLabel.setObjectName(u"indicateurLabel")
        self.indicateurLabel.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.indicateurLabel)

        self.nextPushButton = QPushButton(self.widget)
        self.nextPushButton.setObjectName(u"nextPushButton")

        self.horizontalLayout.addWidget(self.nextPushButton)


        self.retranslateUi(commonTrack)
        self.leavePushButton.clicked.connect(commonTrack.close)

        QMetaObject.connectSlotsByName(commonTrack)
    # setupUi

    def retranslateUi(self, commonTrack):
        commonTrack.setWindowTitle(QCoreApplication.translate("commonTrack", u"Form", None))
        self.titleLabel.setText(QCoreApplication.translate("commonTrack", u"titre", None))
        self.artistLabel.setText(QCoreApplication.translate("commonTrack", u"artist", None))
        self.imageLabel.setText(QCoreApplication.translate("commonTrack", u"images", None))
        self.restartPushButton.setText(QCoreApplication.translate("commonTrack", u"nouvelle playlist", None))
        self.leavePushButton.setText(QCoreApplication.translate("commonTrack", u"quitter!", None))
        self.backPushButton.setText(QCoreApplication.translate("commonTrack", u"pr\u00e8cedent", None))
        self.indicateurLabel.setText(QCoreApplication.translate("commonTrack", u"1/?", None))
        self.nextPushButton.setText(QCoreApplication.translate("commonTrack", u"suivant", None))
    # retranslateUi

