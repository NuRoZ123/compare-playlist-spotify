from PySide6.QtWidgets import QApplication, QMainWindow
from design.code.commonTrack import Ui_commonTrack

class TrackWindow(QMainWindow):
      def __init__(self):
        super(TrackWindow, self).__init__()

        # ici on associe le .ui modélisé avec le designer
        self.ui = Ui_commonTrack()
        self.ui.setupUi(self)
